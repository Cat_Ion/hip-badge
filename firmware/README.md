# Badge development

To develop code on the badge, you need to set up a development environment (using ESP IDF for the compiling and linking), and you need to make sure that when code has been compiled you can flash the badge.

The following instructions are based on the assumption you are using a Linux system. WSL (Windows Subsystem for Linux) will not work, since it is unable to use the USB device.

## 1. Connecting your device

Depending on your distro

```shell
$ sudo tail -f /var/log/syslog | grep tty
```
or

```shell
$ sudo dmesg -w | grep tty
```

Switch your badge on, then connect it to your machine.

The terminal should show some lines giving you a hint at which unix device you will be able to reach your badge. Typically, this would be something like `/dev/ttyACM0`.

Whereever the instructions below say `PORT`, insert `/dev/ttyACM0` or whatever you took from the syslog.

## 2. Checking out the repo

```shell
$ git clone https://gitlab.com/tidklaas/hip-badge
$ git submodule update --init --recursive
$ cd hip-Badge/firmware/blinkenlights
```

## 3. Using ESP IDF natively or containerized

### ESP IDF in a container

You can also use a docker version of it. Create this little shell script, which calls `idf.py` inside docker instead of directly.

Remember to replace `PORT` by the port that your badge appears at `idf.py`

```shell
#!/bin/bash
# mounts your checke
sudo docker run -v $PWD:/project -w /project --device [/dev/PORT] espressif/idf:v4.4.6
```

Call this script instead of `idf.py` later on in the documentation.

### ESP IDF Local installation

Development is done on the v4.4.x branch of the ESP-IDF. If you have not
installed the ESP-IDF, please follow the excellent instructions at
https://docs.espressif.com/projects/esp-idf/en/v4.4.3/esp32c3/get-started/index.html or see below for a docker based installation.

If you already have an installed ESP-IDF, check out the latest v4.4.x tag:

```shell
$ cd $IDF_PATH
$ git fetch
$ git checkout v4.4.6
```

_at this point you might have some residual files lying around in `components/` and `examples/`. Check them with `git status` and remove the not needed ones._

```shell
$ git submodule update --init --recursive
$ bash install.sh
```

## 4. Running/modifying the firmware

To get started with your HiP Badge on a computer with a ESP-IDF installation:

```shell
# proceeding from p2, your workdir is firmware/blinkenlights
$ idf.py set-target esp32c3
$ idf.py menuconfig # --help
# select and enter: Blinkenlights Configuration  --->
```

## 5. Updating Blinkenlights config

Depending on your badge version

### 1 gen

### 2 and 3 gen

## 6. (Optional) Re-flashing firmware

You should now be set up for development. The main file with blinkenlight is located in `main/hipbadge.c`. Make the changes you need, then

```shell
$ idf.py build
```

and if that threw no errors, connect your device and do

```shell
$ idf.py -p [/dev/PORT] flash
```

If this did not work, you can try to do

```shell
$ idf.py -p [/dev/PORT] erase-flash
$ idf.py -p [/dev/PORT] erase-otadata
$ idf.py -p [/dev/PORT] flash
```

Right after flashing, your device should reboot and operate with your changes.
